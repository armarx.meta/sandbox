/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::ArmarXObjects::ActiveStatechartStateTracker
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ActiveStatechartStateTracker.h"
#include "ArmarXCore/core/services/profiler/Profiler.h"

#include <mutex>

using namespace armarx;


bool ActiveStatechartStateTracker::isStateActive(const std::string &name)
{
    std::lock_guard<std::mutex> g{stateEntriesMutex};
    if(stateEntries.count(name))
    {
        return stateEntries.at(name).isActive();
    }
    return false;
}

bool ActiveStatechartStateTracker::hasActiveStates() const
{
    std::lock_guard<std::mutex> g{stateEntriesMutex};
    for(const auto& pair : stateEntries)
    {
        if(pair.second.isActive())
        {
            return true;
        }
    }
    return false;
}

std::vector<std::string> ActiveStatechartStateTracker::getActiveStates()
{
    std::lock_guard<std::mutex> g{stateEntriesMutex};
    std::vector<std::string> result;
    for(const auto& pair : stateEntries)
    {
        if(pair.second.isActive())
        {
            result.emplace_back(pair.first);
        }
    }
    return result;
}

void ActiveStatechartStateTracker::onInitComponent()
{
    for(const auto& topic : topics)
    {
        usingTopic(topic);
    }
}

void ActiveStatechartStateTracker::reportStatechartTransitionList(const ProfilerStatechartTransitionList &transitions, const Ice::Current &)
{
    for(const auto& t :transitions)
    {
        reportStatechartTransition(t);
    }
}

void ActiveStatechartStateTracker::reportStatechartTransition(const ProfilerStatechartTransition &transition, const Ice::Current &)
{
    {
        std::lock_guard<std::mutex> g{stateTypeIsSetMutex};
        if(stateTypeIsSet.count(transition.targetStateIdentifier))
        {
            return;
        }
        stateTypeIsSet.emplace(transition.targetStateIdentifier);
    }
    std::lock_guard<std::mutex> g{stateEntriesMutex};
    auto& e = stateEntries[transition.targetStateIdentifier];
    e.name = transition.targetStateIdentifier;
    e.type = transition.targetStateType;
}

void ActiveStatechartStateTracker::reportEvent(const ProfilerEvent &e, const Ice::Current &)
{
    reportEventList({e});
}

void ActiveStatechartStateTracker::reportEventList(const ProfilerEventList &l, const Ice::Current &)
{
    std::lock_guard<std::mutex> g{stateEntriesMutex};
    for(const ProfilerEvent& e : l)
    {
        const auto& name = e.parentName;
        StateEntry& entr = stateEntries[name];
        entr.name = name;
        entr.addActivationStatus(e.timestamp, e.functionName == Profiler::Profiler::GetEventName(Profiler::Profiler::EventType::eFunctionStart));
    }
}

bool ActiveStatechartStateTracker::StateEntry::AcDeacEntry::operator <(const ActiveStatechartStateTracker::StateEntry::AcDeacEntry &other) const
{
    return std::make_tuple(timestamp, sequenceNumber) < std::make_tuple(other.timestamp, other.sequenceNumber);
}

bool ActiveStatechartStateTracker::StateEntry::isActive(Ice::Long t) const
{
    if(type == eStateType::eFinalState)
    {
        return false;
    }
    bool active = false;
    auto it = activationStatus.begin();
    while(it != activationStatus.end())
    {
        if(it->timestamp > t)
        {
            return active;
        }
        active = it->activated;
        ++it;
    }
    return active;
}

void ActiveStatechartStateTracker::StateEntry::addActivationStatus(Ice::Long timestamp, bool activate)
{
    activationStatus.emplace(AcDeacEntry{timestamp, sequenceNumber++, activate});
}
