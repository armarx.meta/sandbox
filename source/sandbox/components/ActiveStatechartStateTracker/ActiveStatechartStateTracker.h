/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Invasic::ArmarXObjects::ActiveStatechartStateTracker
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_MANAGEDICEOBJECT_Invasic_ActiveStatechartStateTracker_H
#define _ARMARX_MANAGEDICEOBJECT_Invasic_ActiveStatechartStateTracker_H

#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/interface/core/Profiler.h>

#include <mutex>

namespace armarx
{
    /**
     * @class ActiveStatechartStateTracker
     * @brief A brief description
     *
     * Detailed Description
     */
    class ActiveStatechartStateTracker :
        virtual public ManagedIceObject,
        virtual protected ProfilerListener
    {
    public:
        ActiveStatechartStateTracker(const std::vector<std::string>& topics = {"StateReportingTopic", armarx::Profiler::PROFILER_TOPIC_NAME}) : topics{topics}
        {
        }
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "ActiveStatechartStateTracker";
        }

        bool isStateActive(const std::string& name);
        bool hasActiveStates() const;
        std::vector<std::string> getActiveStates();

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent() {}

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent() {}

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent() {}

        const std::vector<std::string> topics;

        virtual void reportStatechartTransitionList(const ProfilerStatechartTransitionList& transitions, const Ice::Current& = GlobalIceCurrent);
        virtual void reportStatechartTransition(const ProfilerStatechartTransition& transition, const Ice::Current& = GlobalIceCurrent);

        virtual void reportEvent(const ProfilerEvent& e, const Ice::Current& = GlobalIceCurrent);
        virtual void reportEventList(const ProfilerEventList& l, const Ice::Current& = GlobalIceCurrent);

        virtual void reportNetworkTraffic(const std::string &, const std::string &, Ice::Int, Ice::Int, const Ice::Current& = GlobalIceCurrent) {}
        virtual void reportStatechartInputParameters(const ProfilerStatechartParameters &, const Ice::Current& = GlobalIceCurrent) {}
        virtual void reportStatechartLocalParameters(const ProfilerStatechartParameters &, const Ice::Current& = GlobalIceCurrent) {}
        virtual void reportStatechartOutputParameters(const ProfilerStatechartParameters &, const Ice::Current& = GlobalIceCurrent) {}
        virtual void reportProcessCpuUsage(const ProfilerProcessCpuUsage &, const Ice::Current& = GlobalIceCurrent) {}
        virtual void reportProcessMemoryUsage(const ProfilerProcessMemoryUsage &, const Ice::Current& = GlobalIceCurrent) {}
        virtual void reportStatechartInputParametersList(const ProfilerStatechartParametersList &, const Ice::Current& = GlobalIceCurrent) {}
        virtual void reportStatechartLocalParametersList(const ProfilerStatechartParametersList &, const Ice::Current& = GlobalIceCurrent) {}
        virtual void reportStatechartOutputParametersList(const ProfilerStatechartParametersList &, const Ice::Current& = GlobalIceCurrent) {}
        virtual void reportProcessCpuUsageList(const ProfilerProcessCpuUsageList &, const Ice::Current& = GlobalIceCurrent) {}
        virtual void reportProcessMemoryUsageList(const ProfilerProcessMemoryUsageList &, const Ice::Current& = GlobalIceCurrent) {}

        struct StateEntry
        {
            std::string name;
            eStateType type {eStateType::eUndefined};

            struct AcDeacEntry
            {
                Ice::Long timestamp;
                Ice::Long sequenceNumber;
                bool activated;
                bool operator <(const AcDeacEntry& other) const;
            };

            bool isActive(Ice::Long t = std::numeric_limits<Ice::Long>::max()) const;
            void addActivationStatus(Ice::Long timestamp, bool activate);
        private:
            Ice::Long sequenceNumber{0};
            std::set<AcDeacEntry> activationStatus;
        };

        mutable std::mutex stateEntriesMutex;
        std::map<std::string, StateEntry> stateEntries;
        mutable std::mutex stateTypeIsSetMutex;
        std::set<std::string> stateTypeIsSet;
    };
}

#endif
