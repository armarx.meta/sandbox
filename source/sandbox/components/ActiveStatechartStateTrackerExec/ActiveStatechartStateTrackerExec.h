/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::ArmarXObjects::ActiveStatechartStateTrackerExec
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_sandbox_ActiveStatechartStateTrackerExec_H
#define _ARMARX_COMPONENT_sandbox_ActiveStatechartStateTrackerExec_H


#include <ArmarXCore/core/Component.h>

namespace armarx
{
    /**
     * @class ActiveStatechartStateTrackerExecPropertyDefinitions
     * @brief
     */
    class ActiveStatechartStateTrackerExecPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ActiveStatechartStateTrackerExecPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @defgroup Component-ActiveStatechartStateTrackerExec ActiveStatechartStateTrackerExec
     * @ingroup sandbox-Components
     * A description of the component ActiveStatechartStateTrackerExec.
     * 
     * @class ActiveStatechartStateTrackerExec
     * @ingroup Component-ActiveStatechartStateTrackerExec
     * @brief Brief description of class ActiveStatechartStateTrackerExec.
     * 
     * Detailed description of class ActiveStatechartStateTrackerExec.
     */
    class ActiveStatechartStateTrackerExec :
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "ActiveStatechartStateTrackerExec";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();
    };
}

#endif
