/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::ArmarXObjects::customtype_selfsender
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/util/StringHelperTemplates.h>

#include "customtype_selfsender.h"

using namespace armarx;


void customtype_selfsender::onInitComponent()
{
    usingTopic("topic");
    offeringTopic("topic");
}


struct tst
{
    int    x = 1 ;
    float  y = 2 ;
    double z = 3 ;
    Eigen::Vector3f v;
};
BOOST_FUSION_ADAPT_STRUCT(tst, (int, x)(float, y)(double, z)(Eigen::Vector3f, v))

void customtype_selfsender::onConnectComponent()
{
#define pf(...) ARMARX_INFO << "fusion_struct_size   "                                                                     \
                            << "fx " << IceSerialization::fusion_struct_size<__VA_ARGS__>::FixedLength  ::value << " \t "  \
                            << "kn " << IceSerialization::fusion_struct_size<__VA_ARGS__>::KnownCategory::value << " \t "  \
                            << "sz " << IceSerialization::fusion_struct_size<__VA_ARGS__>::MinWireSize  ::value << "\t"    \
                            << GetTypeString<__VA_ARGS__>()                                                     << '\n'

#define pfi(I, ...) ARMARX_INFO << "fusion_struct_size " << I << ' '                                                                \
                                << "fx " << IceSerialization::fusion_struct_size<__VA_ARGS__>::FixedLength_I  <I>::value << " \t "  \
                                << "kn " << IceSerialization::fusion_struct_size<__VA_ARGS__>::KnownCategory_I<I>::value << " \t "  \
                                << "sz " << IceSerialization::fusion_struct_size<__VA_ARGS__>::MinWireSize_I  <I>::value << "\t"    \
                                << GetTypeString<IceSerialization::fusion_struct_size<__VA_ARGS__>::type_i<I>>()         << '\n'

#define p(...) ARMARX_INFO << "StreamableTraits     "                                                \
                           << "fx " << IceSerialization::FixedLength  <__VA_ARGS__>::value << " \t " \
                           << "kn " << IceSerialization::KnownCategory<__VA_ARGS__>::value << " \t " \
                           << "sz " << IceSerialization::MinWireSize  <__VA_ARGS__>::value << "\t"   \
                           << GetTypeString<__VA_ARGS__>()                                 << "  \t" \
                           << #__VA_ARGS__                                                 << '\n'

    ARMARX_INFO << ".......................................................................................";
    p(tst);
    pf(tst);
    pfi(0, tst);
    pfi(1, tst);
    pfi(2, tst);
    pfi(3, tst);
    ARMARX_INFO << ".......................................................................................";
    p(int);
    p(float);
    p(double);
    p(Eigen::Vector3f);
    ARMARX_INFO << ".......................................................................................";
    p(const int);
    p(const float);
    p(const double);
    p(const Eigen::Vector3f);
    ARMARX_INFO << ".......................................................................................";
    p(int&);
    p(float&);
    p(double&);
    p(Eigen::Vector3f&);
    ARMARX_INFO << ".......................................................................................";
    p(const int&);
    p(const float&);
    p(const double&);
    p(const Eigen::Vector3f&);
    ARMARX_INFO << ".......................................................................................";
    p(bool);
    p(long);
    p(Eigen::Matrix<float,  6,  6>);
    ARMARX_INFO << ".......................................................................................";
    p(std::vector<int>);
    ARMARX_INFO << ".......................................................................................";
    p(char);
    p(unsigned long);
    ARMARX_INFO << ".......................................................................................";

    const Eigen::Vector3f v1 = Eigen::Vector3f::Ones() * 1;
    const Eigen::Vector3f v2 = Eigen::Vector3f::Ones() * 2;
    const Eigen::Vector3f v3 = Eigen::Vector3f::Ones() * 3;
    Eigen::Matrix3f m3;
    m3 << v1, v2, v3;
    ARMARX_INFO << ".\n" << m3;

    topic = getTopic<selfsender_interfacePrx>("topic");
    std::thread{[this]{
            Eigen::Vector3f v3 = Eigen::Vector3f::Zero();
            Eigen::Matrix<float,  6,  6> mx;
            mx << 11, 12, 13, 14, 15, 16,
               21, 22, 23, 24, 25, 26,
               31, 32, 33, 34, 35, 36,
               41, 42, 43, 44, 45, 46,
               51, 52, 53, 54, 55, 56,
               61, 62, 63, 64, 65, 66;
            custom_struct_via_ice_1 c1;
            c1.i = 42;
            c1.d = 84;
            c1.f = 84.168;
            custom_struct_via_ice_2 c2;
            c2.i = 42;
            c2.d = 84;
            c2.f = 84.168;
            c2.s.emplace_back("string\n01234\t56789\nstring");
            c2.s.emplace_back("qwer");
            while (true)
            {
                ++v3(0);
                v3(1) = static_cast<unsigned>(v3(0)) % 42;
                v3(2) = v3(0) / 42;
                std::this_thread::sleep_for(std::chrono::milliseconds{100});
                ARMARX_IMPORTANT_S << "send \t" << v3.transpose();
                topic->receive(v3, mx, c1, c2);
            }
        }}.detach();
}

void customtype_selfsender::receive(const Eigen::Vector3f& v3,
                                    const ::Eigen::Matrix6f& mx,
                                    const ::armarx::custom_struct_via_ice_1ada& c1,
                                    const ::armarx::custom_struct_via_ice_2ada& c2,
                                    const Ice::Current&)
{
    ARMARX_IMPORTANT << ".                                 got \t" << v3.transpose()
                     << "\n" << mx
                     << "\n" << c1.d << "\t" << c1.f << "\t" << c1.i
                     << "\n" << c2.d << "\t" << c2.f << "\t" << c2.i << "\t" << c2.s;
}






armarx::PropertyDefinitionsPtr customtype_selfsender::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new customtype_selfsenderPropertyDefinitions(
            getConfigIdentifier()));
}

