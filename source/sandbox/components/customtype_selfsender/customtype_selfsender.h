/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::ArmarXObjects::customtype_selfsender
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <thread>
#include <ArmarXCore/core/Component.h>

#include <sandbox/interface/customtype_ice.h>

namespace armarx
{
    /**
     * @class customtype_selfsenderPropertyDefinitions
     * @brief
     */
    class customtype_selfsenderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        customtype_selfsenderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @defgroup Component-customtype_selfsender customtype_selfsender
     * @ingroup sandbox-Components
     * A description of the component customtype_selfsender.
     *
     * @class customtype_selfsender
     * @ingroup Component-customtype_selfsender
     * @brief Brief description of class customtype_selfsender.
     *
     * Detailed description of class customtype_selfsender.
     */
    class customtype_selfsender :
        virtual public armarx::Component,
        virtual public selfsender_interface
    {

    protected:
        void onInitComponent() override;
        void onConnectComponent() override;
        void receive(const ::Eigen::Vector3f& inc,
                     const ::Eigen::Matrix6f& mx,
                     const ::armarx::custom_struct_via_ice_1ada& c1,
                     const ::armarx::custom_struct_via_ice_2ada& c2,
                     const Ice::Current& c = GlobalIceCurrent) override;


        void onDisconnectComponent() override {}
        void onExitComponent() override {}
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


        selfsender_interfacePrx topic;
    public:
        customtype_selfsender() = default;
        std::string getDefaultName() const override
        {
            return "customtype_selfsender";
        }

    };
}
