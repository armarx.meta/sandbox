/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::ArmarXObjects::ice_crash_test
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_sandbox_ice_crash_test_H
#define _ARMARX_COMPONENT_sandbox_ice_crash_test_H


#include <ArmarXCore/core/Component.h>

namespace armarx
{
    /**
     * @class ice_crash_testPropertyDefinitions
     * @brief
     */
    class ice_crash_testPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ice_crash_testPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @defgroup Component-ice_crash_test ice_crash_test
     * @ingroup sandbox-Components
     * A description of the component ice_crash_test.
     * 
     * @class ice_crash_test
     * @ingroup Component-ice_crash_test
     * @brief Brief description of class ice_crash_test.
     * 
     * Detailed description of class ice_crash_test.
     */
    class ice_crash_test :
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "ice_crash_test";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();
    };
}

#endif
