/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::ArmarXObjects::ice_crash_test
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ice_crash_test.h"


using namespace armarx;


void ice_crash_test::onInitComponent()
{

}


void ice_crash_test::onConnectComponent()
{

}


void ice_crash_test::onDisconnectComponent()
{

}


void ice_crash_test::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr ice_crash_test::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new ice_crash_testPropertyDefinitions(
                                      getConfigIdentifier()));
}

