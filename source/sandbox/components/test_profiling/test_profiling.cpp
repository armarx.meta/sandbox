/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::ArmarXObjects::test_profiling
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "test_profiling.h"

#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/IceGridAdmin.h>
#include <Ice/Metrics.h>
#include <Ice/Instrumentation.h>


using namespace armarx;


void test_profiling::onInitComponent()
{

}

std::ostream& operator<<(std::ostream& o, const IceMX::MetricsPtr& metr)
{
    if(metr)
    {
        o<< metr->ice_id();
    }
    else
    {
        o << "nullptr";
    }
    return o;
}

void test_profiling::onConnectComponent()
{
    auto manager = getArmarXManager();
    auto comm = manager->getCommunicator();
    auto admin = comm->getAdmin();
    auto metricsRaw = comm->findAdminFacet("Metrics");
    IceMX::MetricsAdminPtr metrics = IceMX::MetricsAdminPtr::dynamicCast(metricsRaw);

    ARMARX_CHECK_NOT_NULL(metrics);

    Ice::StringSeq diabled;
    ARMARX_INFO << "enabled " << metrics->getMetricsViewNames(diabled);
    ARMARX_INFO << "diabled: " << diabled;



    metrics->enableMetricsView("MyView");

    for(int i = 0; i < 10; ++i)
    {
        Ice::Long time = 0;
        IceMX::MetricsView view = metrics->getMetricsView("MyView",time,GlobalIceCurrent);
        ARMARX_INFO << "###########################\ngot view: \n" << VAROUT(time) << "\nview:"
        <<ARMARX_STREAM_PRINTER
        {
            for(const auto& pair: view)
            {
                out << pair.first <<"\n";
                for(const IceMX::MetricsPtr& metr: pair.second)
                {
                    if(metr)
                    {
                        out<<"---- "<< metr->ice_id()<<"\n";
                    }
                    else
                    {
                        out<< "---- "<<"nullptr"<<"\n";
                    }

                }
            }
        };
        std::this_thread::sleep_for(std::chrono::seconds{1});
    }

}


void test_profiling::onDisconnectComponent()
{

}


void test_profiling::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr test_profiling::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new test_profilingPropertyDefinitions(
                                      getConfigIdentifier()));
}

