/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::ArmarXObjects::CliParamPassing
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_sandbox_CliParamPassing_H
#define _ARMARX_COMPONENT_sandbox_CliParamPassing_H


#include <ArmarXCore/core/Component.h>

namespace armarx
{
    /**
     * @class CliParamPassingPropertyDefinitions
     * @brief
     */
    class CliParamPassingPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        CliParamPassingPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            defineOptionalProperty<std::string>("prop", "FOO", "Foo");
        }
    };

    /**
     * @defgroup Component-CliParamPassing CliParamPassing
     * @ingroup sandbox-Components
     * A description of the component CliParamPassing.
     *
     * @class CliParamPassing
     * @ingroup Component-CliParamPassing
     * @brief Brief description of class CliParamPassing.
     *
     * Detailed description of class CliParamPassing.
     */
    class CliParamPassing :
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "CliParamPassing";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();
    };
}

#endif
