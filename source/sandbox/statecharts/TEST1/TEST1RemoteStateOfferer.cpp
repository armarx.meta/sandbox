/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::TEST1::TEST1RemoteStateOfferer
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TEST1RemoteStateOfferer.h"

using namespace armarx;
using namespace TEST1;

// DO NOT EDIT NEXT LINE
TEST1RemoteStateOfferer::SubClassRegistry TEST1RemoteStateOfferer::Registry(TEST1RemoteStateOfferer::GetName(), &TEST1RemoteStateOfferer::CreateInstance);



TEST1RemoteStateOfferer::TEST1RemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < TEST1StatechartContext > (reader)
{
}

void TEST1RemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void TEST1RemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void TEST1RemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string TEST1RemoteStateOfferer::GetName()
{
    return "TEST1RemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr TEST1RemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new TEST1RemoteStateOfferer(reader));
}



