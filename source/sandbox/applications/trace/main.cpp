/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::application::trace
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */



/*

#include <ArmarXCore/core/logging/Logging.h>

#include <boost/current_function.hpp>
namespace armarx
{

struct SourceLocation
{
    std::uint64_t line;
    std::string   file;
    std::string   func;
};

struct SourceLocation
{
    const std::uint64_t line;
    const std::string   file;
    const std::string   func;
};

#define CURRENT_SOURCE_LOCATION(name) SourceLocation name {__LINE__, __FILE__, BOOST_CURRENT_FUNCTION}


struct LogSenderFakeLoggingLocation
{
    SourceLocation at;
    boost::optional<Ice::Int> threadId; //LogSender::getThreadId();
};

struct Trace
{
    struct Data
    {
        Data(Data&&) = default;
        Data(const Data&) = default;
        const Trace* trace;
        const SourceLocation location;
        const int initialNumberOfUncaughtExceptions;
        const Ice::Int threadId;
        static bool CurrentScopeIsUnwinding()
        {
            return std::uncaught_exceptions() > initialNumberOfUncaughtExceptions;
        }
    };

    enum class PrintMode
    {
        Never,
        WhenUnwindingCurrentScope,
        WhenUnwindingAnyScope,
        Always
    };

    static struct
    {
        std::atomic<Trace::PrintMode> PrintMode;
    } GlobalSettings;

    static thread_local struct
    {
        std::optional<Trace::PrintMode> PrintMode;

        Trace::PrintMode getPrintMode() const
        {
            return PrintMode? PrintMode.get() : Trace::GlobalSettings::PrintMode;
        }
    } LocalSettings;

    static thread_local const std::deque<Data> TraceStack;
};

struct
{
    struct Inner : Trace
    {
        Inner()
        {
            Trace::TraceStack.emplace_back(this, CURRENT_SOURCE_LOCATION(), std::uncaught_exceptions(), LogSender::getThreadId());
            if()
            ARMARX_DEBUG_S << "Enter scope {line " << l.line << " in function '" << l.func << "' in file '" << l.file << "'}\n";
        }
        ~Inner()
        {
            ARMARX_DEBUG_S << "Leave scope {line " << l.line << " in function '" << l.func << "' in file '" << l.file << "'}\n";
        }
        const CURRENT_SOURCE_LOCATION(l); /*this uses the enclosing function name* /
    };
    const Inner i;
} Trace;



}
::armarx::LogSender& operator<<(::armarx::LogSender& log, const ::armarx::LogSenderFakeLoggingLocation& loc)
{
    log.setFile(loc.at.file);
    log.setLine(loc.at.line);
    log.setFunction(loc.at.func);
    return log;
}

#define ARMARX_SCOPE_TRACE                                                                                                              \
    struct                                                                                                                              \
    {                                                                                                                                   \
        struct Inner                                                                                                                    \
        {                                                                                                                               \
            Inner()                                                                                                                     \
            {                                                                                                                           \
                ARMARX_DEBUG_S << "Enter scope {line " << l.line << " in function '" << l.func << "' in file '" << l.file << "'}\n";    \
            }                                                                                                                           \
            ~Inner()                                                                                                                    \
            {                                                                                                                           \
                ARMARX_DEBUG_S << "Leave scope {line " << l.line << " in function '" << l.func << "' in file '" << l.file << "'}\n";    \
            }                                                                                                                           \
            const CURRENT_SOURCE_LOCATION(l); /*this uses the enclosing function name* /                                                 \
        };                                                                                                                              \
        const Inner i;                                                                                                                  \
    } _detail_ARMARX_SCOPE_TRACE_variable_in_line_##__LINE__##_counter_##__COUNTER__


#ifndef ARMARX_SCOPE_TRACE
#define ARMARX_SCOPE_TRACE do{}while(0)
#endif



#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>
*/


int main(int argc, char* argv[])
{
//    return armarx::runSimpleComponentApp < armarx::trace > (argc, argv, "trace");
}
