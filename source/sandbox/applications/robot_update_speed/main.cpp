/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::application::robot_update_speed
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

//#include <ArmarXCore/core/application/Application.h>
//#include <ArmarXCore/core/Component.h>
//#include <ArmarXCore/core/logging/Logging.h>

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Import/RobotImporterFactory.h>
#include <Eigen/Core>

#include <chrono>

std::mt19937 gen{std::random_device{}()};

static constexpr auto file6 = "/home/raphael/repos/Armar6RT/data/Armar6RT/robotmodel/Armar6-SH/Armar6-SH.xml";
static constexpr auto file4 = "/home/raphael/repos/Armar4/data/Armar4/robotmodel/Armar4.xml";
static constexpr std::size_t N = 100000;

int main()
{

    VirtualRobot::RobotImporterFactoryPtr importer = VirtualRobot::RobotImporterFactory::fromFileExtension("xml", NULL);

    if (!importer)
    {
        std::cout << " ERROR while grabbing importer" << std::endl;
        return 1;
    }

    VirtualRobot::RobotPtr r4f1 = importer->loadFromFile(file4, VirtualRobot::RobotIO::eFull);
    VirtualRobot::RobotPtr r4c1 = importer->loadFromFile(file4, VirtualRobot::RobotIO::eCollisionModel);
    VirtualRobot::RobotPtr r4s1 = importer->loadFromFile(file4, VirtualRobot::RobotIO::eStructure);

    VirtualRobot::RobotPtr r4f2 = importer->loadFromFile(file4, VirtualRobot::RobotIO::eFull);
    VirtualRobot::RobotPtr r4c2 = importer->loadFromFile(file4, VirtualRobot::RobotIO::eCollisionModel);
    VirtualRobot::RobotPtr r4s2 = importer->loadFromFile(file4, VirtualRobot::RobotIO::eStructure);

    VirtualRobot::RobotPtr r4fdc = importer->loadFromFile(file4, VirtualRobot::RobotIO::eFull);
    r4fdc->setUpdateVisualization(false);
    r4fdc->setUpdateCollisionModel(false);
    VirtualRobot::RobotPtr r4fdv = importer->loadFromFile(file4, VirtualRobot::RobotIO::eFull);
    r4fdv->setUpdateVisualization(false);


    VirtualRobot::RobotPtr r6f1 = importer->loadFromFile(file4, VirtualRobot::RobotIO::eFull);
    VirtualRobot::RobotPtr r6c1 = importer->loadFromFile(file4, VirtualRobot::RobotIO::eCollisionModel);
    VirtualRobot::RobotPtr r6s1 = importer->loadFromFile(file4, VirtualRobot::RobotIO::eStructure);

    VirtualRobot::RobotPtr r6f2 = importer->loadFromFile(file4, VirtualRobot::RobotIO::eFull);
    VirtualRobot::RobotPtr r6c2 = importer->loadFromFile(file4, VirtualRobot::RobotIO::eCollisionModel);
    VirtualRobot::RobotPtr r6s2 = importer->loadFromFile(file4, VirtualRobot::RobotIO::eStructure);

    VirtualRobot::RobotPtr r6fdc = importer->loadFromFile(file4, VirtualRobot::RobotIO::eFull);
    r6fdc->setUpdateVisualization(false);
    r6fdc->setUpdateCollisionModel(false);
    VirtualRobot::RobotPtr r6fdv = importer->loadFromFile(file4, VirtualRobot::RobotIO::eFull);
    r6fdv->setUpdateVisualization(false);

    auto checkUpdate = [](const VirtualRobot::RobotPtr& r)
    {
        for(const VirtualRobot::RobotNodePtr& node : r->getRobotNodes())
        {
            node->setJointValueNoUpdate(
                std::uniform_real_distribution<float>(node->getJointLimitLo(),node->getJointLimitHi())(gen)
            );
        }
        const auto beg = std::chrono::high_resolution_clock::now();
        r->applyJointValues();
        const auto end = std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::nanoseconds>(end - beg).count();
    };

    auto checkCopy = [](const VirtualRobot::RobotPtr& r1, const VirtualRobot::RobotPtr& r2)
    {
        auto rn1 = r1->getRobotNodes();
        auto rn2 = r2->getRobotNodes();
        const auto beg = std::chrono::high_resolution_clock::now();
        for(std::size_t i = 0; i < rn1.size(); ++i)
        {
            rn2.at(i)->copyPoseFrom(rn1.at(i));
        }
        const auto end = std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::nanoseconds>(end - beg).count();
    };

   // copyPoseFrom

    auto loopChecks = [checkUpdate, checkCopy](
            const VirtualRobot::RobotPtr& r1,
            const VirtualRobot::RobotPtr& r2,
            std::size_t n, std::string pre)
    {
        std::uintmax_t up = 0;
        std::uintmax_t cp = 0;
        auto toGo = n;
        while(toGo--)
        {
            up += checkUpdate(r1);
            cp += checkCopy(r1, r2);
        }
        std::cout << pre << " u " << up << "\t / " << n << "\t = " << up/n << " ns" << std::endl;
        std::cout << pre << " c " << cp << "\t / " << n << "\t = " << cp/n << " ns" << std::endl;
        std::cout << std::endl;
    };

    loopChecks(r4f1, r4f2, N, "4 F->F ");
    loopChecks(r4s1, r4f2, N, "4 S->F ");
    std::cout << "--" << std::endl;
    loopChecks(r4c1, r4c2, N, "4 C->C ");
    loopChecks(r4s1, r4fdv, N, "4 S->c ");
    std::cout << "--" << std::endl;
    loopChecks(r4s1, r4s2, N, "4 S->S ");
    loopChecks(r4f1, r4s2, N, "4 F->S ");
    loopChecks(r4s1, r4fdc, N, "4 S->s ");

    std::cout << "###############" << std::endl;
    loopChecks(r6f1, r6f2, N, "6 F->F ");
    loopChecks(r6s1, r6f2, N, "6 S->F ");
    std::cout << "--" << std::endl;
    loopChecks(r6c1, r6c2, N, "6 C->C ");
    loopChecks(r6s1, r6fdv, N, "4 S->c ");
    std::cout << "--" << std::endl;
    loopChecks(r6s1, r6s2, N, "6 S->S ");
    loopChecks(r6f1, r6s2, N, "6 F->S ");
    loopChecks(r6s1, r6fdc, N, "4 S->s ");
    return 0;
}



