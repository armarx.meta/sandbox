
#include <type_traits>
#include <stdexcept>

#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/variadic/to_seq.hpp>
#include <boost/preprocessor/tuple/elem.hpp>
#include <boost/preprocessor/stringize.hpp>

namespace armarx
{
    template<class T> T& from_string(T&, const std::string&);
    template<class T> T from_string(const std::string& s)
    {
        T tmp;
        from_string(tmp, s);
        return tmp;
    }
}

#define _detail_ARMARX_MAKE_ENUM_decorate_checks(Namespace, Type, ...)  \
    static_assert(                                                      \
        std::is_enum<Namespace :: Type>::value,                         \
        "the type '" #Namespace "::" #Type "' is not an enum!"          \
    );                                                                  \
    __VA_ARGS__                                                         \
    static_assert(true, "dummy enforcing a semicolon")

// ---------------------------------------------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
#define _detail_ARMARX_MAKE_ENUM_TO_STRING_case(r,data/*(namespace,type)*/,elem)                \
    case BOOST_PP_TUPLE_ELEM(0,data) :: BOOST_PP_TUPLE_ELEM(1,data) :: elem :                   \
        return BOOST_PP_STRINGIZE(BOOST_PP_TUPLE_ELEM(1,data)) "::" BOOST_PP_STRINGIZE(elem);

#define _detail_ARMARX_MAKE_ENUM_TO_STRING_fn_name(Namespace, Type)             \
    ::std::string to_string(Namespace :: Type val)

#define _detail_ARMARX_MAKE_ENUM_TO_STRING_fn_body(Namespace, Type, ...)        \
    {                                                                           \
        using BaseT = typename std::underlying_type<Namespace :: Type>::type;   \
        switch(val)                                                             \
        {                                                                       \
            BOOST_PP_SEQ_FOR_EACH(                                              \
                _detail_ARMARX_MAKE_ENUM_TO_STRING_case,                        \
                (Namespace, Type),                                              \
                BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__)                           \
            )                                                                   \
        }                                                                       \
        throw std::invalid_argument{                                            \
            "unknown enum value for '" #Namespace "::" #Type "': " +            \
            to_string(static_cast<BaseT>(val))                             \
        };                                                                      \
    }

// ---------------------------------------------------------------------------------------------- //

#define ARMARX_MAKE_ENUM_TO_STRING_DECLARE(Namespace, Type)             \
    _detail_ARMARX_MAKE_ENUM_decorate_checks(Namespace, Type,           \
        _detail_ARMARX_MAKE_ENUM_TO_STRING_fn_name(Namespace, Type);    \
    )

#define ARMARX_MAKE_ENUM_TO_STRING_DEFINE(Namespace, Type, /*elems*/...)            \
    _detail_ARMARX_MAKE_ENUM_decorate_checks(Namespace, Type,                       \
        _detail_ARMARX_MAKE_ENUM_TO_STRING_fn_name(Namespace, Type)                 \
        _detail_ARMARX_MAKE_ENUM_TO_STRING_fn_body(Namespace, Type, __VA_ARGS__)    \
    )

#define ARMARX_MAKE_ENUM_TO_STRING_INLINE(Namespace, Type, /*elems*/...)     \
    _detail_ARMARX_MAKE_ENUM_decorate_checks(Namespace, Type,                       \
        inline _detail_ARMARX_MAKE_ENUM_TO_STRING_fn_name(Namespace, Type)          \
        _detail_ARMARX_MAKE_ENUM_TO_STRING_fn_body(Namespace, Type, __VA_ARGS__)    \
    )

// ---------------------------------------------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //

#define _detail_ARMARX_MAKE_ENUM_FROM_STRING_case(r,data/*(namespace,type)*/,elem)      \
    if(                                                                                 \
        str ==                                                                          \
        BOOST_PP_STRINGIZE(BOOST_PP_TUPLE_ELEM(1,data)) "::" BOOST_PP_STRINGIZE(elem)   \
    )                                                                                   \
    {                                                                                   \
        target = BOOST_PP_TUPLE_ELEM(0,data) :: BOOST_PP_TUPLE_ELEM(1,data) :: elem;    \
        return target;                                                                  \
    }


#define _detail_ARMARX_MAKE_ENUM_FROM_STRING_fn_name(Namespace, Type, pre)              \
    template<>                                                                          \
    pre Namespace :: Type&                                                              \
    from_string<Namespace :: Type>(Namespace :: Type& target, const std::string& str)

#define _detail_ARMARX_MAKE_ENUM_FROM_STRING_fn_body(Namespace, Type, ...)  \
    {                                                                       \
        BOOST_PP_SEQ_FOR_EACH(                                              \
            _detail_ARMARX_MAKE_ENUM_FROM_STRING_case,                      \
            (Namespace, Type),                                              \
            BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__)                           \
        )                                                                   \
        throw std::invalid_argument{                                        \
            "unknown enum value for enum '" #Namespace "::" #Type           \
            "' and string '" + str + "'"                                    \
        };                                                                  \
    }



// ---------------------------------------------------------------------------------------------- //

#define ARMARX_MAKE_ENUM_FROM_STRING_DECLARE(Namespace, Type)           \
    _detail_ARMARX_MAKE_ENUM_decorate_checks(Namespace, Type,           \
        _detail_ARMARX_MAKE_ENUM_FROM_STRING_fn_name(Namespace, Type,); \
    )

#define ARMARX_MAKE_ENUM_FROM_STRING_DEFINE(Namespace, Type, /*elems*/...)          \
    _detail_ARMARX_MAKE_ENUM_decorate_checks(Namespace, Type,                       \
        _detail_ARMARX_MAKE_ENUM_FROM_STRING_fn_name(Namespace, Type,)              \
        _detail_ARMARX_MAKE_ENUM_FROM_STRING_fn_body(Namespace, Type, __VA_ARGS__)  \
    )

#define ARMARX_MAKE_ENUM_FROM_STRING_INLINE(Namespace, Type, /*elems*/...)          \
    _detail_ARMARX_MAKE_ENUM_decorate_checks(Namespace, Type,                       \
        _detail_ARMARX_MAKE_ENUM_FROM_STRING_fn_name(Namespace, Type, inline)       \
        _detail_ARMARX_MAKE_ENUM_FROM_STRING_fn_body(Namespace, Type, __VA_ARGS__)  \
    )




/*
//#define ARMARX_MAKE_ENUM_FROM_STRING(Namespace, Type, / *elems* /...)                     \
//    static_assert(                                                                      \
//        std::is_enum<Namespace :: Type>::value,                                         \
//        "the type '" #Namespace "::" #Type "' is not an enum!"                          \
//    );                                                                                  \
//    template<>                                                                          \
//    Namespace :: Type&                                                                  \
//    from_string<Namespace :: Type>(Namespace :: Type& target, const std::string& str)   \
//                                                                                       \
//    static_assert(true, "dummy enforcing a semicolon")
*/

// ---------------------------------------------------------------------------------------------- //


// ---------------------------------------------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //

#define ARMARX_MAKE_ENUM_FROM_TO_STRING_DECLARE(...)    \
    ARMARX_MAKE_ENUM_TO_STRING_DECLARE(__VA_ARGS__);    \
    ARMARX_MAKE_ENUM_FROM_STRING_DECLARE(__VA_ARGS__)

#define ARMARX_MAKE_ENUM_FROM_TO_STRING_DEFINE(...)     \
    ARMARX_MAKE_ENUM_TO_STRING_DEFINE(__VA_ARGS__);     \
    ARMARX_MAKE_ENUM_FROM_STRING_DEFINE(__VA_ARGS__)

#define ARMARX_MAKE_ENUM_FROM_TO_STRING_INLINE(...)     \
    ARMARX_MAKE_ENUM_TO_STRING_INLINE(__VA_ARGS__);     \
    ARMARX_MAKE_ENUM_FROM_STRING_INLINE(__VA_ARGS__)




namespace foo
{
    enum class ec {q,w,e}; //ec::w
    enum       nc {q,w,e}; //nc::w
}
#include <iostream>
#include <cassert>
#include <iterator>
#include <string>
#include <cctype>

namespace armarx
{
    ARMARX_MAKE_ENUM_FROM_TO_STRING_INLINE(foo,nc,q,w,e);

    ARMARX_MAKE_ENUM_FROM_TO_STRING_DECLARE(foo,ec);
    ARMARX_MAKE_ENUM_FROM_TO_STRING_DEFINE(foo,ec,q,w,e);
}

using namespace armarx;

int main()
{
    std::cout << to_string(foo::nc::q)<<"\n";
    std::cout << to_string(foo::nc::w)<<"\n";
    std::cout << to_string(foo::nc::e)<<"\n";

    std::cout << to_string(foo::ec::q)<<"\n";
    std::cout << to_string(foo::ec::w)<<"\n";
    std::cout << to_string(foo::ec::e)<<"\n";

    std::cout << to_string(from_string<foo::nc>(to_string(foo::nc::q)))<<"\n";
    std::cout << to_string(from_string<foo::nc>(to_string(foo::nc::w)))<<"\n";
    std::cout << to_string(from_string<foo::nc>(to_string(foo::nc::e)))<<"\n";

    std::cout << to_string(from_string<foo::ec>(to_string(foo::ec::q)))<<"\n";
    std::cout << to_string(from_string<foo::ec>(to_string(foo::ec::w)))<<"\n";
    std::cout << to_string(from_string<foo::ec>(to_string(foo::ec::e)))<<"\n";

    std::cout << BOOST_PP_STRINGIZE(
        ARMARX_MAKE_ENUM_FROM_TO_STRING_INLINE(foo,nc,q,w,e)
    ) << '\n';


}
