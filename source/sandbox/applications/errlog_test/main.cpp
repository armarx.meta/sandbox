/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::application::errlog_test
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


//flag test
struct b
{
    virtual void foo(){}
};
struct c : b
{
    virtual void foo(){}
};


////////////////////////////////////////////////////////////////////////////////
// TO CORE
#include <ArmarXCore/core/util/TemplateMetaProgramming.h>

#include <iostream>

#include <cstdio>
#include<ArmarXCore/core/exceptions/local/ExpressionException.h>


#include <ArmarXCore/core/logging/Logging.h>
#include <cstdint>
#include <string>

using namespace armarx;


/*
char buff[4096];

//struct RtMessageLog : Logging
//{
//    void printMessage(const RtMessageLogEntryBase* msg) const
//    {
//        ARMARX_CHECK_NOT_NULL(msg);
//        if(msg->printMsg)
//        {
//            (checkLogLevel(msg->loggingLevel)) ?
//                _GlobalDummyLogSender :
//                (*loghelper(msg->file().c_str(), msg->line(), msg->func().c_str()))
//                    << msg->loggingLevel
//                    << deactivateSpam(msg->deactivateSpamSec, to_string(msg->deactivateSpamTag_))
//                    << msg->format();
//        }
//    }
//};

//static RtMessageLog log;

//static thread_local RtMessageLog* ErrorLogPtr = new RtMessageLog;// &log;

//#define ARMARX_RT_LOGF(...) _detail_ARMARX_RT_LOGF(__FILE__, ARMARX_FUNCTION,__LINE__, __VA_ARGS__)

//#define _detail_ARMARX_RT_LOGF(file_, func_, line_, FormatString, ...)                                    \
//    [&]{                                                                                            \
//        struct RtMessageLogEntry : RtMessageLogEntryBase                                            \
//        {                                                                                           \
//            using TupleT = decltype(std::make_tuple(__VA_ARGS__));                                  \
//            const TupleT tuple;                                                                     \
//            static RtMessageLogEntryBase* PlacementConstruct(void* place, TupleT tuple)             \
//            {                                                                                       \
//                return new(place) RtMessageLogEntry(std::move(tuple));                              \
//            }                                                                                       \
//            virtual RtMessageLogEntryBase* _placementCopyConstruct(void* place) const final         \
//            {                                                                                       \
//                return new(place) RtMessageLogEntry(*this);                                         \
//            }                                                                                       \
//            virtual std::size_t _sizeInBytes() const final {return sizeof(RtMessageLogEntry);}      \
//            virtual std::size_t _alignof() const final {return alignof(RtMessageLogEntry);}         \
//            virtual std::size_t line() const final {return line_;}                                  \
//            virtual std::string file() const final {return file_;}                                  \
//            virtual std::string func() const final {return func_;}                                  \
//            virtual std::string format() const final {return TupleToStringF(FormatString, tuple);}  \
//        private:                                                                                    \
//            RtMessageLogEntry(TupleT tuple) : tuple{std::move(tuple)} {}                            \
//            RtMessageLogEntry(const RtMessageLogEntry&) = default;                                  \
//        };                                                                                          \
//        ErrorLogPtr->addToLog<RtMessageLogEntry>(__VA_ARGS__);                                      \
//        return 1;                                                                                   \
//    }()


//#define ARMARX_RT_LOGF_DEBUG(...) ARMARX_RT_LOGF(__VA_ARGS__).setLoggingLevel(eDEBUG)
//#define ARMARX_RT_LOGF_VERBOSE(...) ARMARX_RT_LOGF(__VA_ARGS__).setLoggingLevel(eVERBOSE)
//#define ARMARX_RT_LOGF_INFO(...) ARMARX_RT_LOGF(__VA_ARGS__).setLoggingLevel(eINFO)
//#define ARMARX_RT_LOGF_IMPORTANT(...) ARMARX_RT_LOGF(__VA_ARGS__).setLoggingLevel(eIMPORTANT)
//#define ARMARX_RT_LOGF_WARN(...) ARMARX_RT_LOGF(__VA_ARGS__).setLoggingLevel(eWARN)
//#define ARMARX_RT_LOGF_ERROR(...) ARMARX_RT_LOGF(__VA_ARGS__).setLoggingLevel(eERROR)
//#define ARMARX_RT_LOGF_FATAL(...) ARMARX_RT_LOGF(__VA_ARGS__).setLoggingLevel(eFATAL)

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <tuple>
*/
int main()
{

//    ARMARX_RT_LOGF("FOO %d \n",1);


//    ARMARX_RT_LOGF("BAR %d \n",2);

//    std::cout << TupleToStringF("lalala>>%d<<>>%d\n",std::make_tuple(1,133337));

//    std::cout << armarx::GetTypeString<meta::MakeIndexSequence<10>>();

//    return 0;
}
