/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::application::fix_a6_hands
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/MathTools.h>
#include <VirtualRobot/Import/SimoxXMLFactory.h>

int main()
{
    VirtualRobot::RobotPtr robot = VirtualRobot::RobotIO::loadRobot(
                "/home/raphael/repos/Armar6RT/data/Armar6RT/robotmodel/Armar6-SH/Armar6-SH.xml",
                VirtualRobot::RobotIO::eStructure);

    auto p = [&](std::string s)
    {
        const Eigen::Matrix4f tcp = robot->getRobotNode("Hand "+s+" TCP")->getGlobalPose();
        const Eigen::Matrix4f base = robot->getRobotNode("Hand "+s+" Col")->getGlobalPose();

//        const Eigen::Matrix4f t2b = tcp.inverse() * base.inverse();
        const Eigen::Matrix4f t2b = base.inverse() * tcp.inverse();

        std::cout << s << "\n";

        std::cout <<  t2b << "\n";
        Eigen::Vector3f r{0,0,0};
        float alpha = 0;
        VirtualRobot::MathTools::eigen4f2axisangle(t2b, r, alpha);
        std::cout << r.transpose() << " " << alpha << "\n";
    };
    p("L");
    p("R");

}
