/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::application::InheritanceTrace
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//#include <type_traits>

//namespace std
//{
//    template< bool B, class T, class F >
//    using conditional_t = typename conditional<B,T,F>::type;

//    template <bool B>
//    using bool_constant = integral_constant<bool, B>;

//    template<class...> struct conjunction : std::true_type { };
//    template<class B1> struct conjunction<B1> : B1 { };
//    template<class B1, class... Bn>
//    struct conjunction<B1, Bn...>
//        : std::conditional_t<bool(B1::value), conjunction<Bn...>, B1> {};
//    //template<class... B>
//    //inline constexpr bool conjunction_v = conjunction<B...>::value;

//    template<class...> struct disjunction : std::false_type { };
//    template<class B1> struct disjunction<B1> : B1 { };
//    template<class B1, class... Bn>
//    struct disjunction<B1, Bn...>
//        : std::conditional_t<bool(B1::value), B1, disjunction<Bn...>>  { };
//    //template<class... B>
//    //inline constexpr bool disjunction_v = disjunction<B...>::value;

//    template<class B>
//    struct negation : std::bool_constant<!bool(B::value)> { };
//    //template<class B>
//    //inline constexpr bool negation_v = negation<B>::value;
//}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//#include <tuple>
//using InhFlags = int;

//enum class InheritanceType
//{
//    Public,
//    Protected,
//    Private,
//    VirtualPublic,
//    VirtualProtected,
//    VirtualPrivate,
//};


//template<InheritanceType IFlags, class Base, class...Ds>
//struct InheritanceTrace;

//template<class Base, class...Ds> struct InheritanceTrace<InheritanceType::       Public   , Base, Ds...>:         public    Ds... {using BaseTypes = std::tuple<Ds...>; static_assert(std::conjunction<std::is_base_of<Base,Ds>...>::value, "One class is not derived.");};
//template<class Base, class...Ds> struct InheritanceTrace<InheritanceType::VirtualPublic   , Base, Ds...>: virtual public    Ds... {using BaseTypes = std::tuple<Ds...>; static_assert(std::conjunction<std::is_base_of<Base,Ds>...>::value, "One class is not derived.");};
//template<class Base, class...Ds> struct InheritanceTrace<InheritanceType::       Protected, Base, Ds...>:         protected Ds... {using BaseTypes = std::tuple<Ds...>; static_assert(std::conjunction<std::is_base_of<Base,Ds>...>::value, "One class is not derived.");};
//template<class Base, class...Ds> struct InheritanceTrace<InheritanceType::VirtualProtected, Base, Ds...>: virtual protected Ds... {using BaseTypes = std::tuple<Ds...>; static_assert(std::conjunction<std::is_base_of<Base,Ds>...>::value, "One class is not derived.");};
//template<class Base, class...Ds> struct InheritanceTrace<InheritanceType::       Private  , Base, Ds...>:         private   Ds... {using BaseTypes = std::tuple<Ds...>; static_assert(std::conjunction<std::is_base_of<Base,Ds>...>::value, "One class is not derived.");};
//template<class Base, class...Ds> struct InheritanceTrace<InheritanceType::VirtualPrivate  , Base, Ds...>: virtual private   Ds... {using BaseTypes = std::tuple<Ds...>; static_assert(std::conjunction<std::is_base_of<Base,Ds>...>::value, "One class is not derived.");};


//template<class...Ds            > struct InheritanceTrace<InheritanceType::       Public   , void, Ds...>:         public    Ds... {using BaseTypes = std::tuple<Ds...>;};
//template<class...Ds            > struct InheritanceTrace<InheritanceType::VirtualPublic   , void, Ds...>: virtual public    Ds... {using BaseTypes = std::tuple<Ds...>;};
//template<class...Ds            > struct InheritanceTrace<InheritanceType::       Protected, void, Ds...>:         protected Ds... {using BaseTypes = std::tuple<Ds...>;};
//template<class...Ds            > struct InheritanceTrace<InheritanceType::VirtualProtected, void, Ds...>: virtual protected Ds... {using BaseTypes = std::tuple<Ds...>;};
//template<class...Ds            > struct InheritanceTrace<InheritanceType::       Private  , void, Ds...>:         private   Ds... {using BaseTypes = std::tuple<Ds...>;};
//template<class...Ds            > struct InheritanceTrace<InheritanceType::VirtualPrivate  , void, Ds...>: virtual private   Ds... {using BaseTypes = std::tuple<Ds...>;};

////template<class T> struct
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//#include <ArmarXCore/core/application/Application.h>
//#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/util/StringHelpers.h>
//#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

//struct B
//{
//    using i = int;
//    void g(){std::cout<<"g\n";}
//   using BaseTypes = void;
//   void f(){std::cout<<"B   "<<armarx::GetTypeString<BaseTypes>()<<"\n"; }
//};

//struct D1: virtual public InheritanceTrace<InheritanceType::VirtualPublic,B,B>
//{
//   void f(){std::cout<<"D1  "<<armarx::GetTypeString<BaseTypes>()<<"\n"; }
//};
//struct D2: virtual public InheritanceTrace<InheritanceType::VirtualPublic,B,B>
//{
//   void f(){std::cout<<"D2  "<<armarx::GetTypeString<BaseTypes>()<<"\n"; }
//};

//struct DD: virtual public InheritanceTrace<InheritanceType::VirtualPublic,B,B,D1,D2>
//{
//   void f(){std::cout<<"DD  "<<armarx::GetTypeString<BaseTypes>()<<"\n"; }
//};

//#define VRT virtual

//struct QB {};

//struct Q1: VRT QB { char i;};
//struct Q2: VRT public Q1 { };
//struct Q3: VRT public Q1 { };
//struct Q4: VRT public Q2,VRT public Q3{ };
//struct Q5: VRT public Q1,VRT public Q2,VRT public Q3{ };



//struct FN
//{
//    void g(){FN::f();}

//    static void f()
//    {
//        std::cout<<"FN(f) "<<armarx::GetTypeString(f)<<"\n";
//    }
//    int i;
//};
//struct FND : FN
//{

//    static void f()
//    {
//        std::cout<<"FN(f) "<<armarx::GetTypeString(f)<<"\n";
//    }
//};


namespace armarx
{
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct SensorValueBase{
    virtual ~ SensorValueBase()=default;
//    static void GetClassMemberInfo(){}

};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //template<class ClassT, class MemberT>
    //std::string GetDataMemberPointerClassTypeString(MemberT ClassT::*)
    //{
    //    return armarx::GetTypeString<ClassT>();
    //}

    //template<class ClassT, class MemberT>
    //std::string GetDataMemberPointerMemberTypeString(MemberT ClassT::*)
    //{
    //    return armarx::GetTypeString<MemberT>();
    //}

    //template<class ClassT, class MemberT>
    //ClassT DataMemberPointerClassType(MemberT ClassT::*);
    //template<class ClassT, class MemberT>
    //MemberT DataMemberPointerMemberType(MemberT ClassT::*);


    template<class T, class = void> struct DataFieldsInfo;
    //    static std::size_t GetNumberOfFields();
    //    static std::string GetFieldAsString(std::size_t i, T field);
    //    static std::vector<std::string> GetFieldNames();

    template<class T>
    struct DataFieldsInfo<T, typename std::enable_if<std::is_fundamental<T>::value>::type>
    {
        static std::size_t GetNumberOfFields()
        {
            return 1;
        }
        static std::string GetFieldAsString(std::size_t i, T field)
        {
            ARMARX_CHECK_EQUAL(i,0);
            return to_string(field);
        }
        static std::vector<std::string> GetFieldNames() {throw std::logic_error{"Fundamental types have no field names"};}
    };

    namespace detail
    {

    template<class CommonBaseT, class ClassT>
    struct ClassMemberInfo;

    template<class CommonBaseT>
        struct ClassMemberInfoEntry
        {
        public:
            using CommonBaseType = CommonBaseT;
            //general
            const std::string& getClassName() const
            {
                return className;
            }
            const std::string& getMemberName() const
            {
                return memberName;
            }
            const std::string& getMemberTypeName() const
            {
                return memberTypeName;
            }
            //fields
            std::size_t getNumberOfFields() const
            {
                return numberOfFields;
            }
            const std::string& getFieldName(std::size_t i) const
            {
                ARMARX_CHECK_LESS(i, numberOfFields);
                return fieldNames.at(i);
            }
            std::string getFieldAsString(std::size_t i, const CommonBaseT* ptr) const
            {
                ARMARX_CHECK_NOT_NULL(ptr);
                ARMARX_CHECK_LESS(i, numberOfFields);
                return fieldToString.at(i)(*ptr);
            }
            void setFieldNames(std::vector<std::string> names)
            {
                ARMARX_CHECK_EQUAL(names.size(), numberOfFields);
                fieldNames = std::move(names);
            }

            //variants
            //TimedVariantBasePtr toVariant(IceUtil::Time timestamp);

        private:
            template<class ComBaseType, class ClassT>
            friend struct ClassMemberInfo;
            std::string className;
            std::string memberName;
            //elementar subparts
            std::string memberTypeName;
            std::size_t numberOfFields;
            std::vector<std::string> fieldNames;
            std::vector<std::function<std::string(const CommonBaseT*)>> fieldToString;
            //variants
            bool framed{false};
        };
    }

    template<class CommonBaseT, class ClassT>
    struct ClassMemberInfo
    {
        using ClassType = ClassT;
        using CommonBaseType = CommonBaseT;
        static_assert(std::is_base_of<CommonBaseType,ClassType>::value, "The class has to inherit the common base class");
        using Entry = detail::ClassMemberInfoEntry<CommonBaseType>;


        template<class MemberPtrClassType, class MemberType>
        Entry& addMemberVariable(MemberType MemberPtrClassType::* ptr, const std::string& name)
        {
                static_assert(
                    std::is_base_of<ClassType, MemberPtrClassType>::value,
                        "The class of the member pointer has to match the class given as parameter");

            Entry& e = entries[name];
            e.className = GetTypeString<ClassType>();
            e.memberName = name;
            e.memberTypeName = GetTypeString<MemberType>();
            e.numberOfFields = DataFieldsInfo<MemberType>::GetNumberOfFields();
            ARMARX_CHECK_NOT_EQUAL(0, e.numberOfFields);
            //field to string
            {
                e.fieldToString.reserve(e.numberOfFields);
                for(std::size_t i = 0; i < e.numberOfFields; ++i)
                {
                    e.fieldToString.emplace_back(
                                [i,ptr](const CommonBaseType* ptrBase)
                    {
                        const MemberPtrClassType* cptr = dynamic_cast<const MemberPtrClassType*>(ptrBase);
                        ARMARX_CHECK_NOT_NULL(cptr);
                        return DataFieldsInfo<MemberType>::GetFieldAsString(i,cptr->*ptr);
                    }
                    );
                }
            }
            //field names
            {
                e.fieldNames.reserve(e.numberOfFields);
                if(e.numberOfFields == 1)
                {
                    e.fieldNames.emplace_back(e.memberName);
                }
                else
                {
                    for(auto& name : DataFieldsInfo<MemberType>::GetFieldNames())
                    {
                        e.fieldNames.emplace_back(e.memberName + "." + std::move(name));
                    }
                }
            }
            return e;
        }

        template<class BaseClassType>
        void addBaseClass()
        {
            if(std::is_same<BaseClassType, CommonBaseType>::value)
            {
                return;
            }
            static_assert(std::is_base_of<CommonBaseType, BaseClassType>::value, "The base class has to inherit the common base class");
            static_assert(std::is_base_of<BaseClassType, ClassType>::value, "The base class has to be a base class");
            static_assert(!std::is_same<BaseClassType, ClassType>::value, "The base class has must not be the class");

            std::set<std::string> basesAddedInCall;
            const auto&& baseClassInfo = BaseClassType::GetClassMemberInfo();
            if(addedBases.count(baseClassInfo.GetClassName()))
            {
                return;
            }
            for(const auto& pair :baseClassInfo.getEntries())
            {
                const Entry& e = pair.second;
                if(!addedBases.count(e.getClassName()))
                {
                    ARMARX_CHECK_EXPRESSION_W_HINT(
                        !entries.count(e.getMemberName()),
                        "Adding the base class '" << GetTypeString<BaseClassType>()
                        << "' adds an entry for the member '" << e.getMemberName()
                        <<"' which already was added by class '" << entries.at(e.getMemberName()).getClassName()<<"'");
                    basesAddedInCall.emplace(e.getClassName());
                    entries[e.getMemberName()] = e;
                }
            }
            addedBases.insert(basesAddedInCall.begin(),basesAddedInCall.end());
        }

        static const std::string& GetClassName()
        {
            return GetTypeString<ClassType>();
        }
        const std::map<std::string, Entry>& getEntries() const
        {
            return entries;
        }

    private:
        std::string className;
        std::map<std::string, Entry> entries;
        std::set<std::string> addedBases;
    };

    template<class DerivedClass>
    using SensorValueInfo = ClassMemberInfo<SensorValueBase, DerivedClass>;
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////
////////////////////
////////////////////
////////////////////
////////////////////
/// EXAMPLE



#include <ArmarXCore/core/util/TemplateMetaProgramming.h>



namespace armarx
{




    class SensorValue1DoFActuatorPosition : virtual public SensorValueBase
    {
    public:
        static SensorValueInfo<SensorValue1DoFActuatorPosition> GetClassMemberInfo()
        {
            SensorValueInfo<SensorValue1DoFActuatorPosition> svi;
            svi.addMemberVariable(&SensorValue1DoFActuatorPosition::position, "position");
            return svi;
        }
        float position = 0.0f;






        //static_assert(std::is_base_of<CommonBaseClassType, ClassType>::value, "This class has to inherit SensorValueBase");




    };
    class SensorValue1DoFActuatorVelocity : virtual public SensorValueBase
    {
    public:

        float velocity = 0.0f;

        static SensorValueInfo<SensorValue1DoFActuatorVelocity> GetClassMemberInfo()
        {
            SensorValueInfo<SensorValue1DoFActuatorVelocity> svi;
            svi.addMemberVariable(&SensorValue1DoFActuatorVelocity::velocity, "velocity");
            return svi;
        }
    };
    class SensorValue1DoFActuatorAcceleration : virtual public SensorValueBase
    {
    public:

        float acceleration = 0.0f;

        static SensorValueInfo<SensorValue1DoFActuatorAcceleration> GetClassMemberInfo()
        {
            SensorValueInfo<SensorValue1DoFActuatorAcceleration> svi;
            svi.addMemberVariable(&SensorValue1DoFActuatorAcceleration::acceleration, "acceleration");
            return svi;
        }
    };
    class SensorValue1DoFActuatorTorque : virtual public SensorValueBase
    {
    public:

        float torque = 0.0f;

        static SensorValueInfo<SensorValue1DoFActuatorTorque> GetClassMemberInfo()
        {
            SensorValueInfo<SensorValue1DoFActuatorTorque> svi;
            svi.addMemberVariable(&SensorValue1DoFActuatorTorque::torque, "torque");
            return svi;
        }
    };


    class SensorValue1DoFActuator :
        virtual public SensorValue1DoFActuatorPosition,
        virtual public SensorValue1DoFActuatorVelocity,
        virtual public SensorValue1DoFActuatorAcceleration,
        virtual public SensorValue1DoFActuatorTorque
    {
    public:
        float foo = 0;

        ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK(SensorValueHasGetClassMemberInfo, GetClassMemberInfo, SensorValueInfo<T>(*)(void));

        void _detail_check_for_static_GetClassMemberInfo_overload()
        {
            static_assert(SensorValueHasGetClassMemberInfo<std::decay<decltype(*this)>::type>::value,
                          "This class has to implement GetClassMemberInfo() returning an instance of SensorValueInfo<THIS_CLASS_TYPE>");
        }


        static SensorValueInfo<SensorValue1DoFActuator> GetClassMemberInfo()
        {
            SensorValueInfo<SensorValue1DoFActuator> svi;
            svi.addMemberVariable(&SensorValue1DoFActuator::foo, "foo");
            svi.addBaseClass<SensorValue1DoFActuatorPosition>();
            svi.addBaseClass<SensorValue1DoFActuatorVelocity>();
            svi.addBaseClass<SensorValue1DoFActuatorAcceleration>();
            svi.addBaseClass<SensorValue1DoFActuatorTorque>();
            return svi;
        }

    };
}

int main()
{
//    B{}.f();
//    D1{}.f();
//    D2{}.f();
//    DD{}.f();
//        std::cout<<"DONE"<<std::endl;


//        std::cout<<sizeof(Q1)<<std::endl;
//        std::cout<<sizeof(Q2)<<std::endl;
//        std::cout<<sizeof(Q3)<<std::endl;
//        std::cout<<sizeof(Q4)<<std::endl;
//        std::cout<<sizeof(Q5)<<std::endl;

//        std::cout<<"foo   "<<armarx::GetTypeString(&Q1::i)<<"\n";
//        std::cout<<"foo   "<<<<"\n";
//        std::cout<<"FN    "<<armarx::GetTypeString(FN::f)<<"\n";
//        FN::f();

//        std::cout<<"FN(checkT)   "<<(&FND::f == &decltype(ClassOfClassMemberPointer(&FN::i))::f)<<"\n";
//        std::cout<<"FN(checkT)   "<<(&FN::i == &FND::i)<<"\n";
//        std::cout<<"foo   "<<armarx::GetTypeString<decltype(ClassOfClassMemberPointer(&FN::i))>()<<"\n";
//        std::cout<<"foo   "<<armarx::GetTypeString<decltype(ClassOfClassMemberPointer(&FND::i))>()<<"\n";

    armarx::SensorValueInfo<armarx::SensorValue1DoFActuator> svi = armarx::SensorValue1DoFActuator::GetClassMemberInfo();

    std::cout << svi.GetClassName()<<"\n";
    for(const auto& pair : svi.getEntries())
    {
        std::cout<<"    " << pair.first << "\t" << pair.second.getClassName() << "\t" << pair.second.getMemberTypeName()<<"\n";
    }
//    std::cout<<"foo   "<<armarx::GetTypeString<armarx::SensorValue1DoFActuatorPosition::SensorValueInfoType>()<<"\n";

    return 0;
}
