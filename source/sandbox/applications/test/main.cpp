/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::application::test
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


//#include <ArmarXCore/core/application/Application.h>
//#include <ArmarXCore/core/Component.h>
//#include <ArmarXCore/core/logging/Logging.h>

//#include <tuple>
//#include <vector>

//#include <type_traits>
//#include <iostream>

//namespace bar
//{
//    using to_string;

//    template<class T, class = void>
//    struct HasToString : std::false_type {};

//    template<class T>
//    struct HasToString < T, typename std::enable_if<sizeof(to_string(std::declval<T>())) >::type > : std::true_type {};
//}

//struct has0{};
//std::string to_string(has0){return "has0";}

//namespace foo {struct has1{};}
//std::string to_string(foo::has1){return "has1";}

//struct has2{};
//namespace foo {std::string to_string(has2){return "has2";}}

//namespace bar {struct has3{};}
//std::string to_string(bar::has3){return "has3";}

//struct has4{};
//namespace bar {std::string to_string(has4){return "has4";}}

//int main()
//{
////    using bar::to_string;
////    using foo::to_string;
////    using bar::has3;
////    using foo::has1;
//    using namespace foo;
//    using namespace bar;
//    std::cout << "has0  " << bar::HasToString<has0>::value << '\n';
//    std::cout << "has1  " << bar::HasToString<has1>::value << '\n';
//    std::cout << "has2  " << bar::HasToString<has2>::value << '\n';
//    std::cout << "has3  " << bar::HasToString<has3>::value << '\n';
//    std::cout << "has4  " << bar::HasToString<has4>::value << '\n';
//    std::cout << "2string " << to_string(has0{}) << '\n';
//    std::cout << "2string " << to_string(has1{}) << '\n';
//    std::cout << "2string " << to_string(has2{}) << '\n';
//    std::cout << "2string " << to_string(has3{}) << '\n';
//    std::cout << "2string " << to_string(has4{}) << '\n';
//}


#include <type_traits>
#include <iostream>

template< class... >
using void_t = void;

//using namespace std;


struct has0{};
std::string to_string(has0){return "has0";}

namespace foo {struct has1{};}
std::string to_string(foo::has1){return "has1";}

struct has2{};
namespace foo {std::string to_string(has2){return "has2";}}

namespace bar {struct has3{};}
std::string to_string(bar::has3){return "has3";}

struct has4{};
namespace bar {std::string to_string(has4){return "has4";}}



namespace bar
{
    using to_string;

    template<class T, class = void>
    struct HasToString : std::false_type {};

    template<class T>
    struct HasToString < T, void_t<decltype(to_string(std::declval<T>()))>> : std::true_type {};

    void callall()
    {
        using namespace foo;
        using namespace bar;
        std::cout << "2string " << to_string(has0{}) << '\n';
        std::cout << "2string " << ::to_string(has1{}) << '\n';
        std::cout << "2string " << foo::to_string(has2{}) << '\n';
        std::cout << "2string " << ::to_string(has3{}) << '\n';
        std::cout << "2string " << to_string(has4{}) << '\n';
    }


}


int main()
{
    using namespace foo;
    using namespace bar;
    std::cout << "long  " << bar::HasToString<long>::value << '\n';
    std::cout << "has0  " << bar::HasToString<has0>::value << '\n';
    std::cout << "has1  " << bar::HasToString<has1>::value << '\n';
    std::cout << "has2  " << bar::HasToString<has2>::value << '\n';
    std::cout << "has3  " << bar::HasToString<has3>::value << '\n';
    std::cout << "has4  " << bar::HasToString<has4>::value << '\n';
    callall();
}
