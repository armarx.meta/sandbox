/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::application::eigen_ref_test
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <iostream>

#include <boost/current_function.hpp>

#include <Eigen/Core>

void f(const Eigen::Vector3f&){std::cout << BOOST_CURRENT_FUNCTION << std::endl;}

void g(const Eigen::Vector3f&){std::cout << BOOST_CURRENT_FUNCTION << std::endl;}
void g(const Eigen::Vector4f&){std::cout << BOOST_CURRENT_FUNCTION << std::endl;}

void h(const Eigen::Ref<Eigen::Vector3f>&){std::cout << BOOST_CURRENT_FUNCTION << std::endl;}
void h(const Eigen::Ref<Eigen::Vector4f>&){std::cout << BOOST_CURRENT_FUNCTION << std::endl;}

template<class Derived>
std::enable_if_t<std::is_assignable_v<Eigen::Vector3f, Derived>>
i(const Eigen::MatrixBase<Derived>&){std::cout << BOOST_CURRENT_FUNCTION << std::endl;}
template<class Derived>
std::enable_if_t<std::is_assignable_v<Eigen::Vector4f, Derived>>
i(const Eigen::MatrixBase<Derived>&){std::cout << BOOST_CURRENT_FUNCTION << std::endl;}



template<class Targ, class Src>
static constexpr bool is_eigen_assignable_v = Targ::ColsAtCompileTime == Src::ColsAtCompileTime &&
                                              Targ::RowsAtCompileTime == Src::RowsAtCompileTime;

//template<class Targ, class Src>
//static constexpr bool is_eigen_assignable_v =  Eigen::internal::is_convertible<Src, Targ>::value;

template<class Targ, class Src>
using enable_if_eigen_is_assignable_t = std::enable_if_t<is_eigen_assignable_v<Targ, Src>>;



template<class Derived>
enable_if_eigen_is_assignable_t<Eigen::Vector3f, Derived>
j(const Eigen::MatrixBase<Derived>&){std::cout << "Vector3f" << std::endl;}

template<class Derived>
enable_if_eigen_is_assignable_t<Eigen::Vector4f, Derived>
j(const Eigen::MatrixBase<Derived>&){std::cout << "Vector4f" << std::endl;}

template<class Derived>
enable_if_eigen_is_assignable_t<Eigen::Matrix3f, Derived>
j(const Eigen::MatrixBase<Derived>&){std::cout << "Matrix3f" << std::endl;}

template<class Derived>
enable_if_eigen_is_assignable_t<Eigen::Matrix4f, Derived>
j(const Eigen::MatrixBase<Derived>&){std::cout << "Matrix4f" << std::endl;}


int main()
{
    Eigen::Vector3f v3a = Eigen::Vector3f::Zero();
    Eigen::Vector3f v3b = Eigen::Vector3f::Zero();

    Eigen::Vector4f v4a = Eigen::Vector4f::Zero();
    Eigen::Vector4f v4b = Eigen::Vector4f::Zero();

    Eigen::Matrix3f m3a = Eigen::Matrix3f::Zero();
    Eigen::Matrix3f m3b = Eigen::Matrix3f::Zero();

    Eigen::Matrix4f m4a = Eigen::Matrix4f::Zero();
    Eigen::Matrix4f m4b = Eigen::Matrix4f::Zero();

    f(v3a);
//    f(v4a);  // error: eigen assert (different sz)
    f(v3a+v3b);
//    f(v4a+v4b); // error: eigen assert (different sz)

    g(v3a);
    g(v4a);
//    g(a+b); // error: call is ambiguous
//    g(c+d); // error: call is ambiguous

//    h(a); // error: call is ambiguous
//    h(c); // error: call is ambiguous
//    h(a+b); // error: no match
//    h(c+d); // error: no match

//    i(a); // error: call is ambiguous
//    i(c); // error: call is ambiguous
//    i(a+b); // error: call is ambiguous
//    i(c+d); // error: call is ambiguous

    j(v3a);
    j(v3a+v3b);

    j(v4a);
    j(v4a+v4b);

    j(m3a);
    j(m3a+m3b);

    j(m4a);
    j(m4a+m4b);

    j(m3a * v3a);
    j((m3a+m3b) * v3a);

    j(m4a * v4a);
    j((m4a+m4b) * v4a);

    using T = decltype(v3a+v3b);
    using T2 = decltype(Eigen::Vector3f{} = v3a+v3b);
    using T3 = decltype(Eigen::Vector4f{} = v3a+v3b);
    static_assert (std::is_assignable_v<Eigen::Vector3f, T>);
    static_assert (std::is_assignable_v<Eigen::Vector4f, T>);


    return 0;
}









