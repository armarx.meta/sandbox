/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::application::implace_construct_types
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/components/units/RobotUnit/util/HeterogenousContinuousContainer.h>


#define ARMARX_CLASS_MEMBER_INFO_HELPER_BASE(CommonBaseType)                                            \
    ARMARX_PLACEMENT_CONSTRUCTION_HELPER_BASE(CommonBaseType)                                           \
    ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK(                                                              \
        HasGetClassMemberInfo, GetClassMemberInfo,                                                      \
        (::armarx::introspection::ClassMemberInfo<_typeBase, T>(void)));                                \
    using VariantBasePtr = ::armarx::VariantBasePtr;                                                    \
    virtual std::size_t getNumberOfDataFields() const  = 0;                                             \
    virtual std::vector<std::string> getDataFieldNames() const = 0;                                     \
    virtual std::string getDataFieldAsString(std::size_t i) const  = 0;                                 \
    virtual std::map<std::string, VariantBasePtr> toVariants(const IceUtil::Time& timestamp) const = 0;

#define ARMARX_CLASS_MEMBER_INFO_HELPER                                                                 \
    ARMARX_PLACEMENT_CONSTRUCTION_HELPER                                                                \
    using TypeInfo = ::armarx::introspection::ClassMemberInfo<_typeBase, _type>;                        \
    void _check_for_static_GetClassMemberInfo_overload()                                                \
    {                                                                                                   \
        static_assert(HasGetClassMemberInfo<_type>::value,                                              \
                      "This class has to implement GetClassMemberInfo() returning "                     \
                      "an instance of TypeInfo");                                                       \
    }                                                                                                   \
    std::size_t getNumberOfDataFields() const override                                                  \
    {                                                                                                   \
        return TypeInfo::GetNumberOfDataFields();                                                       \
    }                                                                                                   \
    std::vector<std::string> getDataFieldNames() const override                                         \
    {                                                                                                   \
        return TypeInfo::GetDataFieldNames();                                                           \
    }                                                                                                   \
    std::string getDataFieldAsString(std::size_t i) const override                                      \
    {                                                                                                   \
        return TypeInfo::GetDataFieldAsString(this, i);                                                 \
    }                                                                                                   \
    std::map<std::string, VariantBasePtr> toVariants(const IceUtil::Time& timestamp) const override     \
    {                                                                                                   \
        return TypeInfo::ToVariants(timestamp,this);                                                    \
    }



struct testing_base
{
    ARMARX_PLACEMENT_CONSTRUCTION_HELPER_BASE(testing_base)
};
template<class T, std::size_t SZ=1>
struct testing : testing_base
{
    ARMARX_PLACEMENT_CONSTRUCTION_HELPER
    T i[SZ];

    void print()
    {
        using _type = typename std::decay<decltype(*this)>::type;
        std::cout << armarx::GetTypeString<_type>() << VAROUT(_alignof()) << "\n";
        std::cout << armarx::GetTypeString<_type>() << VAROUT(_sizeof()) << "\n\n";
    }
};
struct testing_ : testing_base
{
    ARMARX_PLACEMENT_CONSTRUCTION_HELPER

    void print()
    {
        using _type = typename std::decay<decltype(*this)>::type;
        std::cout << armarx::GetTypeString<_type>() << VAROUT(_alignof()) << "\n";
        std::cout << armarx::GetTypeString<_type>() << VAROUT(_sizeof()) << "\n\n";
    }
};

struct no_virt_fn_no_member
{
    void print()
    {
        using _type = typename std::decay<decltype(*this)>::type;
        std::cout <<  armarx::GetTypeString<_type>() <<VAROUT(alignof(_type)) << "\n";
        std::cout <<  armarx::GetTypeString<_type>() <<VAROUT(sizeof(_type)) << "\n\n";
    }
};

struct one_virt_fn_no_member
{
    virtual void f(){}
    void print()
    {
        using _type = typename std::decay<decltype(*this)>::type;
        std::cout <<  armarx::GetTypeString<_type>() <<VAROUT(alignof(_type)) << "\n";
        std::cout <<  armarx::GetTypeString<_type>() <<VAROUT(sizeof(_type)) << "\n\n";
    }
};

struct no_virt_fn_1b_member
{
    std::uint8_t c{17};
    void print()
    {
        using _type = typename std::decay<decltype(*this)>::type;
        std::cout <<  armarx::GetTypeString<_type>() <<VAROUT(alignof(_type)) << "\n";
        std::cout <<  armarx::GetTypeString<_type>() <<VAROUT(sizeof(_type)) << "\n\n";
    }
};

struct one_virt_fn_1b_member
{
    std::uint8_t c{17};
    virtual void f(){}
    void print()
    {
        using _type = typename std::decay<decltype(*this)>::type;
        std::cout <<  armarx::GetTypeString<_type>() <<VAROUT(alignof(_type)) << "\n";
        std::cout <<  armarx::GetTypeString<_type>() <<VAROUT(sizeof(_type)) << "\n\n";
    }
};

//struct base_class
//{
//    ARMARX_CLASS_MEMBER_INFO_HELPER_BASE(base_class)
//};
//struct derived_class : base_class
//{
//    ARMARX_CLASS_MEMBER_INFO_HELPER
//};

#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

namespace qwer
{
#define virtb
#define virt
//#define virtb virtual
//#define virt virtual


struct SensorValueBase_
{
    ARMARX_PLACEMENT_CONSTRUCTION_HELPER_BASE(SensorValueBase_)
    virtual ~SensorValueBase_() = default;
};

template<class T>
struct SensorValue_ : SensorValueBase_, T
{
    ARMARX_PLACEMENT_CONSTRUCTION_HELPER
    virtual ~SensorValue_() {std::cout << "~" << armarx::GetTypeString(*this)<<"\n";}
};


#undef ARMARX_PLACEMENT_CONSTRUCTION_HELPER_BASE
#define ARMARX_PLACEMENT_CONSTRUCTION_HELPER_BASE(...)

#undef ARMARX_PLACEMENT_CONSTRUCTION_HELPER
#define ARMARX_PLACEMENT_CONSTRUCTION_HELPER

    struct SensorValueBase {ARMARX_PLACEMENT_CONSTRUCTION_HELPER_BASE(SensorValueBase)};

    struct SensorValue1DoFActuatorPosition          : virtb SensorValueBase { ARMARX_PLACEMENT_CONSTRUCTION_HELPER float position = 0.0f; };
    struct SensorValue1DoFActuatorVelocity          : virtb SensorValueBase { ARMARX_PLACEMENT_CONSTRUCTION_HELPER float velocity = 0.0f;    };
    struct SensorValue1DoFActuatorAcceleration      : virtb SensorValueBase { ARMARX_PLACEMENT_CONSTRUCTION_HELPER float acceleration = 0.0f;    };
    struct SensorValue1DoFActuatorTorque            : virtb SensorValueBase { ARMARX_PLACEMENT_CONSTRUCTION_HELPER float torque = 0.0f;    };
    struct SensorValue1DoFGravityTorque             : virtb SensorValueBase { ARMARX_PLACEMENT_CONSTRUCTION_HELPER float gravityTorque = 0.0f;    };
    struct SensorValue1DoFActuatorCurrent           : virtb SensorValueBase { ARMARX_PLACEMENT_CONSTRUCTION_HELPER float motorCurrent = 0.0f;    };
    struct SensorValue1DoFActuatorMotorTemperature  : virtb SensorValueBase { ARMARX_PLACEMENT_CONSTRUCTION_HELPER float motorTemperature = 0.0f;    };
    struct SensorValue1DoFActuatorStatus            : virtb SensorValueBase { ARMARX_PLACEMENT_CONSTRUCTION_HELPER armarx::JointStatus status;    };

    struct SensorValue1DoFActuator :
        virt SensorValue1DoFActuatorPosition,
        virt SensorValue1DoFActuatorVelocity,
        virt SensorValue1DoFActuatorAcceleration,
        virt SensorValue1DoFActuatorTorque,
        virt SensorValue1DoFGravityTorque
    {ARMARX_PLACEMENT_CONSTRUCTION_HELPER};

    struct SensorValue1DoFRealActuator :
        virt SensorValue1DoFActuator,
        virt SensorValue1DoFActuatorCurrent,
        virt SensorValue1DoFActuatorMotorTemperature
    {
        ARMARX_PLACEMENT_CONSTRUCTION_HELPER
        char c0=0;
        float positdion = 0.0f;
        char c1=0;
        char c2=0;
        char c3=0;
    };

}

int main()
{

    std::cout << VAROUT(alignof(armarx::SensorValue1DoFActuatorAcceleration)) << "\n";
    std::cout << VAROUT(sizeof(armarx::SensorValue1DoFActuatorAcceleration)) << "\n\n";



    std::cout << VAROUT(alignof(armarx::SensorValue1DoFRealActuator)) << "\n";
    std::cout << VAROUT(7*sizeof(float)) << "\n";
    std::cout << VAROUT(sizeof(armarx::SensorValue1DoFRealActuator)) << "\n\n";

    std::cout << VAROUT(alignof(qwer::SensorValue1DoFRealActuator)) << "\n";
    std::cout << VAROUT(sizeof(qwer::SensorValue1DoFRealActuator)) << "\n\n";

    std::cout << VAROUT(alignof(qwer::SensorValue_<qwer::SensorValue1DoFRealActuator>)) << "\n";
    std::cout << VAROUT(sizeof(qwer::SensorValue_<qwer::SensorValue1DoFRealActuator>)) << "\n\n";

    std::cout << VAROUT(alignof(armarx::ControlTarget1DoFActuatorVelocity)) << "\n";
    std::cout << VAROUT(sizeof(armarx::ControlTarget1DoFActuatorVelocity)) << "\n\n";

    no_virt_fn_no_member{}.print();
    no_virt_fn_1b_member{}.print();
    one_virt_fn_no_member{}.print();
    one_virt_fn_1b_member{}.print();

    union U
    {
        std::uint64_t p[2];
        one_virt_fn_1b_member c{};
    };
    U u;
    u.c = one_virt_fn_1b_member{};

//    std::cout << VAROUT(std::bitset<64>(u.p[0])) << "\n";
//    std::cout << VAROUT(std::bitset<64>(u.p[1])) << "\n";
    std::cout << std::hex << u.p[0] << "\n";
    std::cout << std::hex << u.p[1] << "\n";

    ++u.c.c;

//    std::cout << VAROUT(std::bitset<64>(u.p[1])) << "\n";
    std::cout << std::hex << u.p[1] << "\n" << std::dec;

    std::cout << VAROUT(alignof(U)) << "\n";
    std::cout << VAROUT(sizeof(U)) << "\n\n";

    std::cout << VAROUT(alignof(testing_base)) << "\n";
    std::cout << VAROUT(sizeof(testing_base)) << "\n\n";

    testing_{}.print();

    testing<std::uint8_t,1>{}.print();
    testing<std::uint8_t,2>{}.print();
    testing<std::uint8_t,4>{}.print();
    testing<std::uint8_t,8>{}.print();
    testing<std::uint8_t,9>{}.print();
    testing<std::uint8_t,10>{}.print();
    testing<std::uint8_t,11>{}.print();
    testing<std::uint8_t,12>{}.print();
    testing<std::uint8_t,13>{}.print();
    testing<std::uint8_t,14>{}.print();
    testing<std::uint8_t,15>{}.print();
    testing<std::uint8_t,16>{}.print();
    testing<std::uint8_t,17>{}.print();
    testing<std::uint8_t,18>{}.print();
    testing<std::uint8_t,19>{}.print();
    testing<std::uint8_t,20>{}.print();
    testing<std::uint8_t,21>{}.print();
    testing<std::uint8_t,22>{}.print();
    testing<std::uint8_t,23>{}.print();
    testing<std::uint8_t,24>{}.print();

    testing<std::uint16_t>{}.print();
    testing<std::uint32_t>{}.print();

    testing<std::uint64_t>{}.print();
    testing<std::uint64_t,2>{}.print();
    testing<std::uint64_t,3>{}.print();
    testing<std::uint64_t,4>{}.print();

    std::cout << VAROUT(sizeof(int)) << "\n\n";

    std::cout << "###################\n";
#define sz(c) \
    std::cout \
    << "cap " << cont.getUsedStorageCapacity() << " (" << cont.getRemainingStorageCapacity() << ") / " << cont.getStorageCapacity() << "\telm "    \
    << cont.getElementCount() << " (" << cont.getElementCapacity() << ") / " << cont.getElementCapacity() << c;

#define check(T) {      \
    T t{};              \
    sz("\t"); std::cout    \
    << VAROUT(sizeof(T)) << "\t"<< VAROUT(t._sizeof()) << "\t" \
    << cont.pushBack(t)<<"\n";}

    armarx::HeterogenousContinuousContainer<qwer::SensorValueBase_> cont;


    sz("\n")
    {
        cont.setElementCapacity(20);
        std::uint8_t data[100];
        cont.assignStorage(static_cast<void*>(data),static_cast<void*>(data + 100));
    }
    sz("\n")
    cont.setElementCapacity(0);
    cont.setStorageCapacity(0);
    sz("\n")
    cont.setElementCapacity(2);
    cont.setStorageCapacity(20);
    sz("\n")

    using big = qwer::SensorValue_<qwer::SensorValue1DoFRealActuator>;
    using small = qwer::SensorValue_<qwer::SensorValue1DoFActuatorPosition>;


    check(big)
    check(big)
    check(small)
    check(small)


    std::cout << "###################\n";
    {
        armarx::HeterogenousContinuousContainer<qwer::SensorValueBase_> cont2 = cont;
        {armarx::HeterogenousContinuousContainer<qwer::SensorValueBase_> cont3{ std::move(cont2)};}
        std::cout << "<<<<<<<<<<<\n";

    }
    std::cout << "###################\n";

    int i = 0;
    std::cout << ++i << "\t" << ++i << "\t" << ++i << "\t" << ++i << "\t" << ++i << "\n";
}
