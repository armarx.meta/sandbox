/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::application::proptest
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>

namespace armarx
{

    struct B: armarx::ComponentPropertyDefinitions
    {
        B(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("B", "B", "B");
        }
    };

    struct A: B
    {
        A(std::string prefix):
            B(prefix)
        {
            defineOptionalProperty<std::string>("A", "A", "A");
        }
    };

}

using namespace armarx;

int main()
{
    IceUtil::Handle<A> a = new A("A");
    std::cout << a->getPrefix();
    std::cout << a->getPropertyValues("PRE##  ");

}
