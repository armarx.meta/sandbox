/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::application::make_eigen_vec_x
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <iostream>
#include <eigen3/Eigen/Core>


template<class ScalarType>
inline Eigen::Matrix<ScalarType, Eigen::Dynamic, 1> MakeEigenVectorX(std::initializer_list<ScalarType> ilist)
{
    Eigen::Matrix<ScalarType, Eigen::Dynamic, 1> r(ilist.size());
    std::size_t i = 0;
    for(const auto e : ilist)
    {
        r(i++) = e;
    }
    return r;
}

template<class ScalarType, class...Ts>
inline Eigen::Matrix<ScalarType, Eigen::Dynamic, 1> MakeEigenVectorXWarnNarrowing(Ts&&...ts)
{
    return MakeEigenVectorX<ScalarType>(std::initializer_list<ScalarType>{std::forward<Ts>(ts)...});
}
template<class ScalarType, class...Ts>
inline Eigen::Matrix<ScalarType, Eigen::Dynamic, 1> MakeEigenVectorXIgnoreNarrowing(Ts&&...ts)
{
    return MakeEigenVectorX<ScalarType>(std::initializer_list<ScalarType>{static_cast<ScalarType>(std::forward<Ts>(ts))...});
}
template<class ScalarType, class...Ts>
inline Eigen::Matrix<ScalarType, Eigen::Dynamic, 1> MakeEigenVectorX(Ts&&...ts)
{
    return MakeEigenVectorX<ScalarType>(std::initializer_list<ScalarType>{static_cast<ScalarType>(std::forward<Ts>(ts))...});
}


int main()
{


    std::cout << MakeEigenVectorXWarnNarrowing<float>( 1,2,3.,4.,5.f ) << "\n";
    std::cout << MakeEigenVectorXIgnoreNarrowing<float>( 1,2,3.,4.,5.f ) << "\n";
    std::cout << MakeEigenVectorX<float>( 1,2,3.,4.,5.f ) << "\n";
    std::cout << MakeEigenVectorX<float>({1,2,3.,4.,5.f}) << "\n";
    return 0;
}

//template <bool...> struct bool_pack;
//template <bool... v>
//using all_true = std::is_same<bool_pack<true, v...>, bool_pack<v..., true>>;

//template <class... Doubles, class = std::enable_if_t<
//    all_true<std::is_convertible<Doubles, double>{}...>{}
//>>
//void foo(Doubles... args) {}
