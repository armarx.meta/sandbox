

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <iostream>

#include <MMM/Motion/Motion.h>

int main()
{
    std::cout << sizeof(MMM::Motion);
    return 0;
}
