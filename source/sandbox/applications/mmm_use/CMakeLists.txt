armarx_component_set_name("mmm_useApp")

find_package(MMMCore QUIET)

find_package(Eigen3 QUIET)



#armarx_build_if(MMMCore_FOUND "MMMCORE not available")
#armarx_build_if(Eigen3_FOUND "Eigen3 not available")


set(COMPONENT_LIBS
    ArmarXCoreInterfaces
    ArmarXCore
    MMMCore
)

include_directories(
    ${Eigen3_INCLUDE_DIR}
   # ${MMMCORE_INCLUDE_DIRS}
 #   ${MMMCore_INCLUDE_DIRS}
)

set(EXE_SOURCE main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")

get_property(inc TARGET mmm_useAppRun PROPERTY INCLUDE_DIRECTORIES)
message(">>> ${inc}")
