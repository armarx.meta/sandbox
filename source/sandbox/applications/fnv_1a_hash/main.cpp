#include <cstdint>

namespace FNV
{

    static constexpr std::uint64_t FNV_offset_basis = 0xcbf29ce484222325;
    static constexpr std::uint64_t FNV_prime = 0x100000001b3;
    //hash = FNV_offset_basis
    //for each byte_of_data to be hashed
    //     hash = hash XOR byte_of_data
    //     hash = hash × FNV_prime
    //return hash


    inline constexpr std::uint64_t FNV_1a_recursice(const char* cstr, std::uint64_t hash = FNV_offset_basis)
    {
        return *cstr ?
                    FNV_1a_recursice(
                        cstr + 1,
                        (hash ^ static_cast<std::uint64_t>(*cstr)) * FNV_prime
                    ):
                    hash;
    }

#if __cplusplus >= 201402L
    inline constexpr std::uint64_t FNV_1a(const char* cstr)
    {
        std::uint64_t hash = FNV_offset_basis;
        for(; *cstr; ++cstr)
        {
            hash ^= static_cast<std::uint64_t>(*cstr);
            hash *= FNV_prime;
        }
        return hash;
    }
    static_assert(FNV_1a(__FILE__) == FNV_1a_recursice(__FILE__), "FNV_1a broken");
#else
    inline constexpr std::uint64_t FNV_1a(const char* cstr) { return FNV_1a_recursice(cstr); }
#endif
}

using namespace FNV;

#include <iostream>

template<std::uint64_t h>
void f()
{
    std::cout << h <<"\n";
}

#include <boost/current_function.hpp>


int main()
{
    static constexpr auto fi  = __FILE__;
    f<FNV_1a(fi)>();

    //static constexpr auto fu  = __func__;
    //f<FNV_1a(fu)>();

    //static constexpr auto bfu = BOOST_CURRENT_FUNCTION;
    //f<FNV_1a(bfu)>();

    return 0;
}
