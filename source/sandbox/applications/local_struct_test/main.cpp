#include <cstdint>
static constexpr std::uint64_t FNV_offset_basis = 0xcbf29ce484222325;
static constexpr std::uint64_t FNV_prime = 0x100000001b3;
inline constexpr std::uint64_t FNV_1a(const char* cstr, std::uint64_t hash = FNV_offset_basis)
{
    return *cstr ?
                FNV_1a(
                    cstr + 1,
                    (hash ^ static_cast<std::uint64_t>(*cstr)) * FNV_prime
                ):
                hash;
}

//#include <ArmarXCore/core/application/Application.h>
//#include <ArmarXCore/core/Component.h>
//#include <ArmarXCore/core/logging/Logging.h>

#include <boost/current_function.hpp>
#include <iostream>

#include <string>

#define log() log_expand(__LINE__, __COUNTER__)
#define log_expand(...) log_i(__VA_ARGS__)
#define log_i(l,c) \
    struct _detail_foo_ ## l ## _ ## c \
    { \
        void f() {std::cout << BOOST_CURRENT_FUNCTION <<"\n";} \
    }; \
    _detail_foo_ ## l ## _ ## c ().f()


template<std::uint64_t FH>
struct fh_template;


int main()
{

    struct
    {
        void f() {std::cout << BOOST_CURRENT_FUNCTION <<"\n";}
    } q;
    q.f();



    struct _detail_foo
    {
        void f() {std::cout << BOOST_CURRENT_FUNCTION <<"\n";}


//        template<class T> void f() {std::cout << BOOST_CURRENT_FUNCTION <<"\n";}
//        template<class T>  struct s{};

    };
    _detail_foo().f();


//    template<>
//    struct fh_template<FNV_1a(__FILE__)>
//    {
//        void f() {std::cout << BOOST_CURRENT_FUNCTION <<"\n";}
//    };
//    fh_template<FNV_1a(__FILE__)>().f();

    log();
    log();

    return 0;
}
