armarx_component_set_name("eigen_alignApp")

find_package(Eigen3 QUIET)
armarx_build_if(Eigen3_FOUND "Eigen3 not available")
include_directories(${Eigen3_INCLUDE_DIR})

set(COMPONENT_LIBS
    ArmarXCoreInterfaces
    ArmarXCore
)

set(EXE_SOURCE main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")
