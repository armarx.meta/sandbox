/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    sandbox::application::eigen_align
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <Eigen/Core>
#include <ArmarXCore/core/logging/Logging.h>

using ev = Eigen::Vector4f;

struct v
{
    ev v;
};

struct vc
{
    ev v;
    char c;
};

struct cv
{
    char c;
    ev v;
};

int main()
{
    std::cout <<"c  " <<sizeof(char)<< "  al " << alignof(char) << "\n";
    std::cout <<"ev " <<sizeof(ev)<< " al " << alignof(ev) << "\n";
    std::cout <<"v  " <<sizeof(v)<< " al " << alignof(v) <<"\n";

    std::cout <<"vc " <<sizeof(vc)<< " al " << alignof(vc) <<"\n";
    std::cout <<"    v/c " << offsetof(vc,v) << " / " << offsetof(vc,c) <<"\n";

    std::cout <<"cv " <<sizeof(cv)<< " al " << alignof(cv) <<"\n";
    std::cout <<"    v/c " << offsetof(cv,v) << " / " << offsetof(cv,c) <<"\n";
}
