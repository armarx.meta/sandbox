#include <boost/mpl/range_c.hpp>

#include <boost/fusion/adapted.hpp>
#include <boost/fusion/include/at.hpp>
#include <boost/fusion/include/for_each.hpp>
// ///////////////////////////////////////////////////////////////////////// //
namespace detail
{
    template<class Sequence, std::size_t I>
    struct sz_i
    {
        static constexpr std::size_t size = sizeof(decltype(boost::fusion::at_c<I>(std::declval<Sequence>())));
    };


    template<class Sequence, std::size_t I>
    struct acc
    {
        static constexpr std::size_t size = sz_i < Sequence, I - 1 >::size + acc < Sequence, I - 1 >::size;
    };
    template<class Sequence>
    struct acc<Sequence, 0>
    {
        static constexpr std::size_t size = 0;
    };
}

template <class Sequence>
struct fusion_struct_size
{
public:
    static constexpr std::size_t number_of_elements = boost::fusion::result_of::size<Sequence>::type::value;
    static constexpr std::size_t size = detail::acc<Sequence, number_of_elements>::size;
    using size_type = std::integral_constant<std::size_t, size>;
};

// ///////////////////////////////////////////////////////////////////////// //
#include <iostream>
template <typename Sequence>
struct VarPrinter
{
    VarPrinter(const Sequence& seq): seq_(seq) {}
    const Sequence& seq_;
    template <typename Index>
    constexpr void operator()(Index idx) const
    {
        //use `Index::value` instead of `idx` if your compiler fails with it
        std::string field_name = boost::fusion::extension::struct_member_name<Sequence, Index::value>::call();
        std::cout << "Var " << field_name << "\t Val " << boost::fusion::at<Index>(seq_) << '\n';
    }
};

template <typename Sequence>
struct VarPrinter2
{
    VarPrinter2(const Sequence& seq, int i): seq_(seq), i {i} {}
    const Sequence& seq_;
    int i;
    template <typename Index>
    void operator()(Index idx) const
    {
        //use `Index::value` instead of `idx` if your compiler fails with it
        std::string field_name = boost::fusion::extension::struct_member_name<Sequence, Index::value>::call();
        std::cout << i << "Var " << field_name << "\t Val " << boost::fusion::at<Index>(seq_) << '\n';
    }
};

template<template<class> class Visitor, class Sequence, class...Params>
void ApplyIndexVisitor(Sequence const& v, Params&& ...params)
{
    using Indices = boost::mpl::range_c<unsigned, 0, boost::fusion::result_of::size<Sequence>::value >;
    boost::fusion::for_each(Indices(), Visitor<Sequence>(v, std::forward<Params>(params)...));
}
// ///////////////////////////////////////////////////////////////////////// //
struct foo
{
    char   c = '1';
    int    x = 1 ;
    float  y = 2 ;
    double z = 3 ;
};

BOOST_FUSION_ADAPT_STRUCT(foo, (char, c)(int, x)(float, y)(double, z))

struct bar
{
    char   c = '1';
    int    x = 1 ;
    float  y = 2 ;
    double z = 3 ;
};

template<class T, class = void>
struct has_boost_fusion_adaption : std::false_type {};

//template<class T>
//struct has_boost_fusion_adaption
//    <
//    T,
//std::void_t<decltype(fusion_struct_size<T>)>
//>
//: std::true_type {};

// ///////////////////////////////////////////////////////////////////////// //
int main()
{
    std::cout << has_boost_fusion_adaption<foo>::value << '\n';
    std::cout << has_boost_fusion_adaption<bar>::value << '\n';
    std::cout << sizeof(foo) << '\n';
    std::cout << sizeof(char) + sizeof(int) + sizeof(float) + sizeof(double) << '\n';
    std::cout << fusion_struct_size<foo>::size << '\n';

    ApplyIndexVisitor<VarPrinter>(foo {});
    ApplyIndexVisitor<VarPrinter2>(foo {}, 2);
}
