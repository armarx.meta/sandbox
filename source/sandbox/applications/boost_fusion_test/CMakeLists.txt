armarx_component_set_name("boost_fusion_testApp")

set(COMPONENT_LIBS)

set(EXE_SOURCE main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")
