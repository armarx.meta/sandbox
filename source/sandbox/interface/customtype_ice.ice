#pragma once

#include <ArmarXCore/interface/serialization/Eigen.ice>
#include <ArmarXCore/interface/serialization/BoostFusionAdaptor.ice>
[["cpp:include:sandbox/interface/Adapted/custom_struct_via_ice.h"]]

module armarx
{
    ARMARX_MAKE_STRUCT_ADAPTOR_ICE(custom_struct_via_ice_1ada, ::armarx::custom_struct_via_ice_1);
    ARMARX_MAKE_STRUCT_ADAPTOR_ICE(custom_struct_via_ice_2ada, ::armarx::custom_struct_via_ice_2);
//    [ "cpp:type:::armarx::custom_struct_via_ice_1" ] sequence<byte> custom_struct_via_ice_1ada;
//    [ "cpp:type:::armarx::custom_struct_via_ice_2" ] sequence<byte> custom_struct_via_ice_2ada;
    interface selfsender_interface
    {
        void receive(
            ::Eigen::Vector3f q,
            ::Eigen::Matrix6f w,
            custom_struct_via_ice_1ada c1,
            custom_struct_via_ice_2ada c2);
    };

};
